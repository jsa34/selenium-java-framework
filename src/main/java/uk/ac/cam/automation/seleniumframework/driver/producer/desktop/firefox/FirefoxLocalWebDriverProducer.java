package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.firefox;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import uk.ac.cam.automation.seleniumframework.driver.producer.BaseLocalDriver;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class FirefoxLocalWebDriverProducer extends BaseLocalDriver implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        if (browserVersion != null) {
            WebDriverManager.firefoxdriver().browserVersion(browserVersion).setup();
        } else {
            WebDriverManager.firefoxdriver().setup();
        }
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability(FirefoxDriver.MARIONETTE, true);
        firefoxOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        return new FirefoxDriver(firefoxOptions);
    }

}
