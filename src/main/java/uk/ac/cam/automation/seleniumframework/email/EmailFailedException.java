package uk.ac.cam.automation.seleniumframework.email;

public class EmailFailedException extends RuntimeException {
    public EmailFailedException(String message) {
        super(message);
    }

    public EmailFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
