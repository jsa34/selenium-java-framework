package uk.ac.cam.automation.seleniumframework.properties;

public class PropertiesFileNotFoundException extends RuntimeException {

    public PropertiesFileNotFoundException(Throwable cause) {
        super(cause);
    }

}
