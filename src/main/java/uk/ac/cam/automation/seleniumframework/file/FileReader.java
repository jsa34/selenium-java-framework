package uk.ac.cam.automation.seleniumframework.file;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class FileReader {

    /**
     * Loads a resource from the class loader that loaded this class which will normally be the same ClassLoader that you want!
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String readFileFromResources(String path) throws IOException {
        return readFileFromResources(path, FileReader.class.getClassLoader());
    }

    /**
     * Loads a resource from a specified class loader.
     *
     * @param path
     * @param classLoader
     * @return
     * @throws IOException
     */
    public static String readFileFromResources(String path, ClassLoader classLoader) throws IOException {
        URL resource = classLoader.getResource(path);
        return FileUtils.readFileToString(new File(resource.getPath()), "UTF-8");
    }

}
