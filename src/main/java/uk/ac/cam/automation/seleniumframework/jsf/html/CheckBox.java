package uk.ac.cam.automation.seleniumframework.jsf.html;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import uk.ac.cam.automation.seleniumframework.site.Site;

public class CheckBox {
    public static void select(String checkboxId) {
        Site.webDriverWait().until(ExpectedConditions.presenceOfElementLocated(By.id(checkboxId)));
        Site.click(By.xpath("//*[@id=\"" + checkboxId + "\"]/.."));
    }

}
