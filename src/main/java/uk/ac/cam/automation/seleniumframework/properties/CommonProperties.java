package uk.ac.cam.automation.seleniumframework.properties;

public class CommonProperties {

    public final static String BROWSER_TYPE = "browser.type";
    //For BrowserStack only:
    public final static String BROWSER_PLATFORM = "browser.platform";
    public final static String BROWSER_PLATFORM_VERSION = "browser.platform.version";
    public final static String BROWSER_VERSION = "browser.version";

    public final static String PROJECT_NAME = "project.name";

    public static final String SCREENSHOT_OFF = "screenshot.off";

    public final static String SELENIUM_GRID_URL = "selenium.grid.url";
    public final static String SELENIUM_DRIVER_WAIT_TIMEOUT = "selenium.driver.wait.timeout";

    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_RETRIEVAL_PROTOCOL = "mail.retrieval.protocol";
    public static final String MAIL_RETRIEVAL_PORT = "mail.retrieval.port";
    public static final String MAIL_TIMEOUT_IN_SECONDS = "mail.timeout.seconds";

}
