package uk.ac.cam.automation.seleniumframework.driver.producer;

import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class BaseLocalDriver {
    protected final static String browserVersion = PropertyLoader.getProperty(CommonProperties.BROWSER_VERSION);
}