package uk.ac.cam.automation.seleniumframework.email.domain;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Email {
    private final List<Recipient> recipients;
    private final String from;
    private final String subject;
    private final String body;
    private final List<InputStream> attachments;

    public Email(Recipient recipient, String from, String subject, String body, List<InputStream> attachments) {
        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        this.recipients = recipients;
        this.from = from;
        this.subject = subject;
        this.body = body;
        this.attachments = attachments;
    }

    public boolean hasAttachments() {
        return this.attachments.size() > 0;
    }

    public List<Recipient> getRecipients() {
        return this.recipients;
    }

    public String getFrom() {
        return this.from;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getBody() {
        return this.body;
    }

    public List<InputStream> getAttachments() {
        return this.attachments;
    }

}
