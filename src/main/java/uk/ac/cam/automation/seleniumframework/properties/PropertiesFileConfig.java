package uk.ac.cam.automation.seleniumframework.properties;

public class PropertiesFileConfig {

    private String fileName;

    public PropertiesFileConfig(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
