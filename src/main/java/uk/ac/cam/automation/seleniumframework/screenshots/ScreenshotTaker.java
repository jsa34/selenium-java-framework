package uk.ac.cam.automation.seleniumframework.screenshots;

import io.qameta.allure.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.driver.DriverMode;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ScreenshotTaker {
    private static final boolean isScreenshotOff = Boolean.parseBoolean(PropertyLoader.getProperty(CommonProperties.SCREENSHOT_OFF));

    public static void attachScreenshot() {
        if (!isScreenshotOff && DriverMode.isRemote()) {
            Allure.getLifecycle().addAttachment(
                    LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yy_hh:mm:ss")),
                    "image/png",
                    "png",
                    getWebDriverScreenshot()
            );
        }
    }

    private static byte[] getWebDriverScreenshot() {
        return (((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES));
    }

}
