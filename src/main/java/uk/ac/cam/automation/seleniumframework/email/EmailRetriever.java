package uk.ac.cam.automation.seleniumframework.email;

import com.google.common.base.Function;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.support.ui.FluentWait;
import uk.ac.cam.automation.seleniumframework.email.domain.Email;
import uk.ac.cam.automation.seleniumframework.email.domain.Recipient;
import uk.ac.cam.automation.seleniumframework.log.Log;

import javax.mail.*;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class EmailRetriever extends BaseEmailService {

    /**
     * Returns an Email object of the latest email from a greenmail inbox. It assumes that the username and password of the account is the same as the email address (which is the default behaviour of greenmail).
     *
     * @param emailAddress Fully formed email of the account under test
     * @return Email
     */
    public static Email getLatestMailFromAccount(String emailAddress) {
        return getLatestMailFromAccount(emailAddress, false);
    }

    /**
     * Returns an Email object of the latest email from a greenmail inbox. It assumes that the username and password of the account is the same as the email address (which is the default behaviour of greenmail).
     *
     * @param emailAddress            Fully formed email of the account under test
     * @param deleteAllAfterRetrieval Delete all messages in the email address's inbox after retrieving the latest message.
     * @return Email
     */
    public static Email getLatestMailFromAccount(String emailAddress, boolean deleteAllAfterRetrieval) {

        Store store = null;
        try {
            store = openMailClient();

            new FluentWait<>(new EmailRetriever.StoreConnectionWrapper(store, emailAddress)).withTimeout(Duration.ofSeconds(timeoutInSeconds)).until(new AccountReady());

            InboxFolder inbox = new InboxFolder(store.getFolder("Inbox"));
            inbox.getInbox().open(Folder.READ_ONLY);

            Message message = Stream.of(inbox.getInbox().getMessages()).min(messageComparator).orElse(null);
            List<String> fromList = new ArrayList<>();
            Address[] fromAddresses;
            if (message != null) {
                fromAddresses = message.getFrom();
            } else {
                throw new MailRetrievalException("From list was empty.");
            }
            for (Address address : fromAddresses) {
                fromList.add(address.toString());
            }
            String from = fromList.stream().findFirst().orElse(null);
            String messageBody = MimeMessageToStringConverter.getBodyTextFromMessage(message);

            Email email = new Email(
                    new Recipient(emailAddress, emailAddress),
                    from,
                    message.getSubject(),
                    messageBody,
                    getAttachments(message)
            );

            if (deleteAllAfterRetrieval) {
                EmailCleaner.removeAllMessages(emailAddress);
            }

            return email;
        } catch (IOException | MessagingException e) {
            throw new MailRetrievalException("Could not retrieve messages from account " + emailAddress + " from host: " + mailHost + " on port: " + port, e);
        } finally {
            if (store != null) {
                try {
                    store.close();
                    Log.Info("Closed the connection to the mail server");
                } catch (MessagingException e) {
                    Log.Warn("Could not close connection to mail server: " + e.getMessage());
                }
            }
        }
    }

    public static List<InputStream> getAttachments(Message message) throws MessagingException {
        Object content = null;
        try {
            content = message.getContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (content instanceof String)
            return null;

        if (content instanceof Multipart) {
            Multipart multipart = (Multipart) content;
            List<InputStream> result = new ArrayList<>();

            for (int i = 0; i < multipart.getCount(); i++) {
                try {
                    result.addAll(getAttachments(multipart.getBodyPart(i)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return result;

        }
        return null;
    }

    private static List<InputStream> getAttachments(BodyPart part) throws IOException, MessagingException {
        List<InputStream> result = new ArrayList<>();
        Object content = part.getContent();
        if (content instanceof InputStream || content instanceof String) {
            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()) || StringUtils.isNotBlank(part.getFileName())) {
                result.add(part.getInputStream());
                return result;
            } else {
                return new ArrayList<>();
            }
        }

        if (content instanceof Multipart) {
            Multipart multipart = (Multipart) content;
            for (int i = 0; i < multipart.getCount(); i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                result.addAll(getAttachments(bodyPart));
            }
        }
        return result;
    }

    private static class StoreConnectionWrapper {

        public Store store;

        public String emailAddress;

        public StoreConnectionWrapper(Store store, String emailAddress) {
            this.store = store;
            this.emailAddress = emailAddress;
        }
    }

    private static class AccountReady implements Function<StoreConnectionWrapper, Boolean> {

        @Override
        public Boolean apply(StoreConnectionWrapper storeConnectionWrapper) {

            try {
                if (!Objects.requireNonNull(storeConnectionWrapper).store.isConnected()) {
                    storeConnectionWrapper.store.connect(mailHost, storeConnectionWrapper.emailAddress, storeConnectionWrapper.emailAddress);
                }
                InboxFolder inbox = new InboxFolder(storeConnectionWrapper.store.getFolder("Inbox"));
                inbox.getInbox().open(Folder.READ_ONLY);
                return inbox.getInbox().getMessages().length > 0;
            } catch (MessagingException e) {
                return false;
            }

        }

    }

}