package uk.ac.cam.automation.seleniumframework.driver;

public enum BrowserType {

    ChromeLocal,
    ChromeLocalNoSandbox,
    ChromeRemote,
    ChromeMobileEmulationLocal,
    ChromeMobileEmulationRemote,
    ChromeEuropeanLocaleRemote,
    AppiumWindowsTenRemote,
    AppiumMobile,
    EdgeLocal,
    EdgeRemote,
    FirefoxLocal,
    FirefoxLocalDebug,
    FirefoxRemote,
    FirefoxEagerLoadRemote,
    FirefoxRemoteDebug,
    InternetExplorerLocal,
    InternetExplorerNativeEventsRemote,
    InternetExplorerRemote,
    OperaLocal,
    OperaRemote,
    SafariLocal,
    SafariRemote,
    SafariTechPreviewLocal,
    SafariTechPreviewRemote
}