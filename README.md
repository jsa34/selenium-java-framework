# Selenium-Java-Allure Framework

A Maven framework in which to build Selenium tests written in Java with Allure reports of test results.

## Getting Started

Copy the repo into your local machine.

### Run tests locally

Right click the feature file and select "Run" or "Debug" to start the test.

### Run tests through the commandline

As this project uses Maven, we can invoke the tests using Maven goals.

To run the test, use your CI or point Maven to the project and use the goals:

```
clean install site
```

### Defining the browser

By default, the project will default to ChromeLocal (running a local Chrome instance) if no browser is specified.

To express a specific browser type, at runtime or through VM options in your IDE, pass the following property:

```
-Dbrowser.type=FirefoxLocal
```

##### Common Parameter Values

| Browser type                    | System property value      |
|:--------------------------------|:---------------------------|
|Chrome (Local)                   | ChromeLocal                |
|Chrome (Remote)                  | ChromeRemote               |
|Firefox (Local)                  | FirefoxLocal               |
|Firefox (Remote)                 | FirefoxRemote              |
|Opera (Local)                    | OperaLocal                 |
|Opera (Remote)                   | OperaRemote                |
|Safari (Local)                   | SafariLocal                |
|Safari (Remote)                  | SafariRemote               |
|Edge (Local)                     | EdgeLocal                  |
|Edge (Remote)                    | EdgeRemote                 |
|Edge (Local)                     | EdgeLocal                  |
|Edge (Remote)                    | EdgeRemote                 |
|Internet Explorer (Local)        | InternetExplorerLocal      |
|Internet Explorer (Remote)       | InternetExplorerRemote     |
|Chrome Mobile Emulation (Local)  | ChromeMobileEmulationLocal |
|Chrome Mobile Emulation (Remote) | ChromeMobileEmulationRemote|

##### Additional Parameter Values

| Browser type                | System property value              |
|:----------------------------|:-----------------------------------|
|Chrome No Sandbox (Local)    | ChromeLocalNoSandbox               |
|Chrome EU Locale (Remote)    | ChromeEuropeanLocaleRemote         |
|Firefox Debug (Local)        | FirefoxLocalDebug                  |
|Firefox Eager Load (Local)   | FirefoxEagerLoadRemote             | *** Used for when page load completion hangs in Firefox.
|IE (Native Events) (Remote)  | InternetExplorerNativeEventsRemote |
|Safari Tech Preview (Local)  | SafariTechPreviewLocal             |
|Safari Tech Preview (Remote) | SafariTechPreviewRemote            |

##### Appium

When writing tests using this framework, you can easily leverage these tests to run against an Appium grid with the following settings:

| Browser type                | System property value              |
|:----------------------------|:-----------------------------------|
| Appium Mobile               | AppiumMobile                       |

You can set Appium properties using the following to create a request (for capability values information, see http://appium.io/docs/en/writing-running-appium/caps/) :
- appium.browser.name
- appium.device.name
- appium.platform.name
- appium.os.version

There is a special pre-set Appium driver for WinAppDriver that can be used for Windows desktop testing by using:

| Browser type                | System property value              |
|:----------------------------|:-----------------------------------|
|Windows Driver (WinAppDriver)| AppiumWindowsTenRemote             |

For more Appium driver capabilities information, see: http://appium.io/docs/en/writing-running-appium/caps/

## Grid Test Execution

When running tests on a remote grid, you must specify a "Remote" type browser, e.g. ChromeRemote:

```
-Dbrowser.type=ChromeRemote
```

##### Executing tests on a Selenium Grid

To point your tests to a Selenium grid, at runtime or through VM options in your IDE, pass the following property:

```
-Dselenium.grid.url=http://localhost:4444/wd/hub
```

##### Executing test on Browserstack

To point your tests to Browserstack at runtime or through VM options in your IDE, pass the following properties:

```
-Dbrowserstack.username=TESTUSER
-Dbrowserstack.access.key=KEY123!
```

To override Browserstack logging levels from their default to turning all logs off, pass the following property:

```
-Dbrowserstack.logging=OFF
```

To override Browserstack logging levels from their default to turning all logs on and at error level, pass the following property:

```
-Dbrowserstack.logging=ON
```

## Writing tests

To write tests, you can call any Webdriver methods by calling:
```
DriverManager.getDriver()
```

This will allow you access all the available methods to all Webdrivers as outlined by the W3C standard.

##### Site class
This is a "helper" class that takes the most common interactions and wraps them with some additional behaviour to avoid common pitfalls (e.g. waiting for element to be ready for an interaction before trying to interact with it) and/or reduce repetition of code.

The Site class has methods such as Site.click(), which has a default wait for the element to be clickable before attempting to click.

##### Primefaces* classes

Additionally, there is a niche set of methods for Primefaces to interact with elements with the same philosophy as Site for general webpage interactions.

##  Properties

Properties are retrieved by default from a file called **test-automation.properties**, which should be located under:

src > main > resources > test-automation.properties

The properties declared here can be overriden, however, if specified as a system property at runtime.

For example, if you declare the following property in your **test-automation.properties** file:

```
browser.type=FirefoxLocal
```

And you also pass the following at runtime:

```
-Dbrowser.type=EdgeLocal
```

The property browser.type will be set to EdgeLocal, as the system property overrides the properties file's values.

## Database for test data

The framework provides a Spring-JDBC backed JDBC template connecting to a database and retrieving data for tests.

To use, create a database driver-backed DAO class that extends BaseDao and override the constructDatasource() method.  Below is an example if using an Oracle database:

```
import oracle.jdbc.pool.OracleDataSource;
import uk.ac.cam.automation.seleniumframework.database.BaseDao;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import javax.sql.DataSource;
import java.sql.SQLException;

public class OracleDao extends BaseDao {

    @Override
    public DataSource constructDatasource() {
        try {
            OracleDataSource dataSource = new OracleDataSource();
            dataSource.setURL(PropertyLoader.getProperty("jdbc.url"));
            dataSource.setUser(PropertyLoader.getProperty("jdbc.username"));
            dataSource.setPassword(PropertyLoader.getProperty("jdbc.password"));
            return dataSource;
        } catch (SQLException e) {
            throw new CreateDataSourceException(e);
        }
    }
}

```


N.B. This implementation only supports SQL databases (e.g. Oracle, MariaDB, etc.) and not NoSQL databases (such as MongoDB).  You will have to implement these in your local project, if needed.

## Email in tests

There is a built-in capability for retrieving emails and using the content in test execution.

To connect to an in-memory mail host (such as Greenmail), you need to set the following properties (for example in test-automation.properties):

```
mail.host=mailhost.internal.com
mail.retrieval.protocol=imap
mail.retrieval.port=5959
mail.timeout.seconds=60
```

Below is an example usage of this to read an email, close the account and delete all emails after reading and then extract the link from the email for navigation in the browser:

```
String latestMailFromAccount = EmailRetriever.getLatestMailFromAccount(email, true).getBody();
System.out.println(latestMailFromAccount);

Document doc = Jsoup.parse(latestMailFromAccount);
Elements links = doc.select("a");
Site.goToURL(links.attr("href"));
```

##  Logging

Logging can be easily added to tests by using the static methods from the Log class.

Available logging levels are:

|Logging Level|Method                                       |
|:------------|:--------------------------------------------|
|Info         |Log.Info("This is an info level message");   |
|Warn         |Log.Warn("This is a warning level message"); |
|Error        |Log.Info("This is an error level message");  |
|Debug        |Log.Info("This is a debug level message");   |

## Reporting

The default reporting provided by the framework is Allure.
By default, screenshots are taken after each step and after a failure (if any), which will display on each step of the report.
To turn this behaviour off and disable screenshots, pass the following property:

```
-Dscreenshot.off=TRUE
```

To take additional screenshots, you can call the following method at any point, and it will automatically add a screenshot to the relevant step:

```
ScreenshotTaker.attachScreenshot();
```

N.B. Although Allure is the default reporter, the TestNG/JUnit reports generated can be used with any compatible reporter in your project if you choose not to use Allure.


## Built With

* [Selenium](https://github.com/SeleniumHQ/selenium) - Browser automation framework
* [Maven](https://maven.apache.org/) - Dependency management
* [TestNG](https://github.com/cbeust/testng) - Testing framework
* [Allure](https://github.com/allure-framework) - Reporting framework
* [WebDriverManager](https://github.com/bonigarcia/webdrivermanager) - Local driver binary management

## Licence

This project is licensed under the MIT License.